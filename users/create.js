'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk'); 
const user = require('./user');
const encryptor = require('./encryptor');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.create = (event, context, callback) => 
{
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);
  
  if (!user.IsValid(data.userName)) 
  {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create the user item.',
    });
    return;
  }
  
  if (!user.IsPasswordValid(data.userPassword)) 
  {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create the user item.',
    });
    return;
  }
  
  var passwordhashAndSalt = encryptor.Encrypt(data.userPassword)
  
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: uuid.v1(),
      userName: data.userName,
      userPasswordHash: passwordhashAndSalt.passwordHash,
      userPasswordSalt: passwordhashAndSalt.salt,
      createdAt: timestamp,
      updatedAt: timestamp,
      emailIsVerified: false
    },
  };

  // write the user to the database
  dynamoDb.put(params, (error) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'text/plain' },
        body: 'Couldn\'t create the user item.',
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify(params.Item.id),
    };
    callback(null, response);
  });
};