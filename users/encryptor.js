'use strict';

const crypto = require('crypto');
function genRandomString (length)
{
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex')    /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
};
function sha512(password, salt)
{
    var hash = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
};
module.exports.Encrypt = (data) => 
{            
    var retObject = {};
    try
    {
        var salt = genRandomString(16); /** Gives us salt of length 16 */
        var retObject = sha512(data, salt);
    }
    catch (ex)
    {
        throw new Error(ex.message);
    }
    return retObject;
}
module.exports.ReEncrypt = (salt, data) =>
{
    var retObject = {};
    try
    {
        var retObject = sha512(data, salt);
    }
    catch (ex)
    {
        throw new Error(ex.message);
    }
    return retObject;
}