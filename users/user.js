'use strict';

module.exports.IsValid = (userName) => 
{    
    var isValid = false;
    
    if (typeof userName === 'string' && 
        userName.indexOf('@') !== -1)
    {
        isValid = true;
    }
    
    return isValid;
};
module.exports.IsPasswordValid = (userPassword) => 
{    
    var isValid = false;
    
    if (typeof userPassword === 'string' &&
        userPassword.length > 8 &&
        userPassword.length < 25)
    {
        isValid = true;
    }
    
    return isValid;
};