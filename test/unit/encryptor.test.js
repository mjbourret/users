'use strict';

const assert = require('assert');
const encryptor = require('../../users/encryptor');

describe('Given a password', function(){
    describe('When encrypting with the same salt', function() {
        it('Then the resulting hash should be the same', function () {
            var examplePassword = '76tu*&ST1P~z';
            
            var firstHashNSalt = encryptor.Encrypt(examplePassword);
            var secondHashNSalt = encryptor.ReEncrypt(firstHashNSalt.salt, examplePassword);
            
            assert.equal(firstHashNSalt.passwordHash, secondHashNSalt.passwordHash);
        });
    });
});
describe('Given a password', function(){
    describe('When encrypting with the same password multiple times', function() {
        it('Then the salt and hash should be diferent each time', function () {
            var examplePassword = '76tu*&ST1P~z';
            
            var firstHashNSalt = encryptor.Encrypt(examplePassword);
            var secondHashNSalt = encryptor.Encrypt(examplePassword);
            
            assert.notEqual(firstHashNSalt.passwordHash, secondHashNSalt.passwordHash);
            assert.notEqual(firstHashNSalt.salt, secondHashNSalt.salt);
        });
    });
});