'use strict';

const assert = require('assert');
const user = require('../../users/user');

describe('Given a user', function(){
    describe('When verifying username format AND when username does not contain @', function() {
        it('Then the user IsValid check should return false', function () {
            assert.equal(user.IsValid('testuser.com'), false);
        });
    });
});
describe('Given a user', function(){
    describe('When verifying username format AND when username does contain @', function() {
        it('Then the user IsValid check should return true', function () {
            assert.equal(user.IsValid('test@user.com'), true);
        });
    });
});
describe('Given a user', function(){
    describe('When verifying username format AND when username is not of type string', function() {
        it('Then the user IsValid check should return false', function () {
            assert.equal(user.IsValid({username: 'test@user.com'}), false);
        });
    });
});
describe('Given a users password', function(){
    describe('When password is less than 9', function() {
        it('Then the user IsPasswordValid check should return false', function () {
            assert.equal(user.IsPasswordValid('12345678'), false);
        });
    });
});
describe('Given a users password', function(){
    describe('When password is greater than 24', function() {
        it('Then the user IsPasswordValid check should return false', function () {
            assert.equal(user.IsPasswordValid('1234567890123456789012345'), false);
        });
    });
});
describe('Given a users password', function(){
    describe('When password is not of string type', function() {
        it('Then the user IsPasswordValid check should return false', function () {
            assert.equal(user.IsPasswordValid({password: '1234567890123456789012345'}), false);
        });
    });
});
describe('Given a users password', function(){
    describe('When password is less than 25 AND greater than 8', function() {
        it('Then the user IsPasswordValid check should return true', function () {
            assert.equal(user.IsPasswordValid('123456789012345678901234'), true);
        });
    });
});