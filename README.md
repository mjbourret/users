# Users micro-service

Support for basic CRUD and functions as **owner** of user data.

### Setup

```
npm install
```

### Deploy

```
severless deploy
```

### Test

```
npm run unit-tests
```
or watch
```
npm run watch-unit-tests
```

### API Gateway Endpoints

PUT:

```
https://mbn6q7x8uf.execute-api.us-east-1.amazonaws.com/dev/users
```
Requirements: 
* _POST_ method 
* Headers "JSON (application/json)"
* User information in body per example below
    
Example: 
```
{
    "userName": "test1@gmail.com",
    "userPassword": "abc123dfg"
}
```

TODO:
* Add version number as query string param to endpoint
* Implement a delete lambda for users
* Implement a get lambda for users
* Implement an update lambda for users
